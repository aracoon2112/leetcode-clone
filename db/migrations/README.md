First migration, create db:
```
psql -h <hostname or ip address> -p <port number of remote machine> -d <database name which you want to connect> -U <username of the database server>
```
In my case: 
```
psql -h 127.0.0.1 -p 5432  -U postgres
```

Create database: 

```
    create database leetcode
    create user migrate with encrypted password 'password'
    grant all privileges on database leetcode to migrate
```