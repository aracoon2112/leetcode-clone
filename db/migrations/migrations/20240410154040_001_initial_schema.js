/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    /**
     * Create the users table
     * users table will have the following columns:
     * - user_id: UUID primary key
     * - name: string
     * - email: string
     * - password: string
     * - created_at: timestamp
     * - updated_at: timestamp
     */

    await knex.schema.createTable('users', (table) => {
        table.uuid('user_id').primary();
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('password').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    });

    /**
     * Create the problems table
     * problems table will have the following columns:
     * - problem_id: UUID primary key
     * - title: string
     * - description: string
     */
    await knex.schema.createTable('problems', (table) => {
        table.uuid('problem_id').primary();
        table.string('title').notNullable();
        table.string('description').notNullable();
    });

    /**
     * Create the submissions table
     * submissions table will have the following columns:
     * - submission_id: UUID primary key
     * - problem_id: UUID foreign key
     * - user_id: UUID foreign key
     * - code: string
     * - status: string
     * - created_at: timestamp
     */
    await knex.schema.createTable('submissions', (table) => {
        table.uuid('submission_id').primary();
        table.uuid('problem_id').notNullable().references('problem_id').inTable('problems');
        table.uuid('user_id').notNullable().references('user_id').inTable('users');
        table.string('code').notNullable();
        table.string('status').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
    });

};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
    
    await knex.raw(`DROP TABLE IF EXISTS submissions`);
    await knex.raw(`DROP TABLE IF EXISTS problems`);
    await knex.raw(`DROP TABLE IF EXISTS users`);
    
};
